//
//  Effect.h
//  Distorsions
//
//  Created by Arthur Masson on 23/01/2014.
//
//

#ifndef __Distorsions__Effect__
#define __Distorsions__Effect__

#include <iostream>
#include "ofxCv.h"
#include "ofMain.h"

class Effect {
public:
    Effect(string name):name(name),drawEffectImage(true)
    {
        parameters.setName(name);
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage) {}
    virtual void draw() {}
    
    string name;
    ofParameterGroup parameters;
    bool drawEffectImage;
    
	ofParameter<int> *hueRange;
	ofParameter<int> *hue;
	ofParameter<int> *saturation;
	ofParameter<int> *brightness;
    
    ofColor color;
};

class NoEffect : public Effect {
public:
    NoEffect():Effect("No effect")
    {
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
    {
        cv::cvtColor(silhouetteImage, effectImage, CV_GRAY2BGR);
    }
};

class Contours3 : public Effect {
public:
    Contours3():Effect("Three contours")
    {
        parameters.add(inOut.set("In/Out", false));
        parameters.add(size.set("Contour size", 10, 1, 15));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<int> size;
	ofParameter<bool> inOut;
};

class ContoursN : public Effect {
public:
    ContoursN():Effect("N contours")
    {
        parameters.add(inOut.set("In/Out", false));
        parameters.add(mode.set("Mode", 1, 0, 2));
        parameters.add(size.set("Contour size", 3, 1, 15));
        parameters.add(n.set("Number of contour", 5, 1, 20));
        parameters.add(offset.set("Offset", 10, 0, 20));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<int> size;
	ofParameter<int> n;
	ofParameter<int> offset;
	ofParameter<int> mode;
	ofParameter<bool> inOut;
};

class ContoursBox : public Effect {
public:
    ContoursBox():Effect("Contours box")
    {
        parameters.add(mode.set("Mode", 1, 0, 6));
        parameters.add(precision.set("Precision", 2, 1, 25));
        parameters.add(size.set("Size", 10, 1, 100));
        parameters.add(thickness.set("Thickness", 1, -1, 50));
        parameters.add(mask.set("Mask", 0, 0, 4));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<int> thickness;
	ofParameter<int> size;
	ofParameter<int> precision;
	ofParameter<int> mode;
    ofParameter<int> mask;
};

class ContoursSpikes : public Effect {
public:
    ContoursSpikes():Effect("Spikes")
    {
        parameters.add(inOut.set("In/Out", false));
        parameters.add(sizeMin.set("Spike min size", 1.0, 0.0, 15.0));
        parameters.add(sizeMax.set("Spike max size", 5.0, 0.0, 15.0));
        parameters.add(amount.set("Amount", 0.25, 0.0, 1.0));
        parameters.add(mode.set("Mode", 0, 0, 2));
        parameters.add(shape.set("Shape", 0, 0, 5));
        parameters.add(shapeFrequency.set("Shape frequency", 0.01, 0.0, 500.0));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<float> sizeMin;
	ofParameter<float> sizeMax;
	ofParameter<float> amount;
	ofParameter<int> mode;
	ofParameter<int> shape;
	ofParameter<float> shapeFrequency;
	ofParameter<bool> inOut;

protected:
    vector<vector<cv::Point> > contours;
};

class ContoursSmooth : public Effect {
public:
    ContoursSmooth():Effect("Smooth")
    {
        drawEffectImage = false;
        parameters.add(mode.set("Mode", 1, 0, 5));
        parameters.add(precision.set("Precision", 2, 1, 25));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void draw();
    
	ofParameter<int> precision;
	ofParameter<int> mode;
    vector<cv::Point> contour;
};

class ContoursLiner : public Effect {
public:
    ContoursLiner():Effect("Liner")
    {
        parameters.add(mode.set("Mode", 1, 0, 5));
        parameters.add(precision.set("Precision", 20, 1, 25));
        parameters.add(circleSize.set("Circle size", 5, 1, 25));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<int> precision;
	ofParameter<int> mode;
	ofParameter<int> circleSize;
    vector<cv::Point> contour;
};

class VideoBlend : public Effect {
public:
    VideoBlend():Effect("VideoBlend")
    {
        nUpdate = 0;
        parameters.add(mode.set("Mode", 1, 0, 5));
        parameters.add(persistence.set("Persistence", 0.5, 0.0, 1.0));
        parameters.add(speed.set("Speed", 10, 1, 100));
        parameters.add(intensity.set("Intensity", 2, 0, 10));
    }
    
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<float> persistence;
	ofParameter<int> speed;
	ofParameter<int> mode;
	ofParameter<int> intensity;
    cv::Mat prevSilhouetteImage;
    int nUpdate;
};

#endif /* defined(__Distorsions__Effect__) */
