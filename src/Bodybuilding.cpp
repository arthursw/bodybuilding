#include "Bodybuilding.h"


//--------------------------------------------------------------
void Bodybuilding::setup() {
    
    ofSetBackgroundAuto(false);
    // --- OF --- //
    
	ofSetFrameRate(60);
	mainOutputSyphonServer.setName("Screen Output");
    
    // --- Effects --- //
    
    effects.push_back(new NoEffect());
//    effects.push_back(new VideoBlend());
//    effects.push_back(new OndesPorteesShader());
    effects.push_back(new Wave());
//    effects.push_back(new StarSphere());
    effects.push_back(new StarFlow());
//    effects.push_back(new ContoursLiner());
//    effects.push_back(new Contours3());
//    effects.push_back(new ContoursN());
//    effects.push_back(new ContoursBox());
//    effects.push_back(new ContoursSpikes());
//    effects.push_back(new ContoursSmooth());
    effects.push_back(new ParticlesOptFlow());
//    effects.push_back(new ParticlesRain());
//    effects.push_back(new ParticlesGrid());

//    effects.push_back(new ParticlesCircles());
    
    // --- GUI --- //
    
    angle.addListener(this,&Bodybuilding::angleChanged);
    effect.addListener(this,&Bodybuilding::effectChanged);
    pause.addListener(this,&Bodybuilding::pauseToggled);
    
    gui.setup("panel"); // most of the time you don't need a name but don't forget to call setup
    gui.add(fpsLabel.set( "FPS", ""));
    gui.add(mode.set( "Mode", 0, 0, 3 ));
    gui.add(effect.set( "Effect", 1, 0, effects.size()-1 ));
    gui.add(effectName.set("Effect name", effects[effect]->name));
    gui.add(pause.set( "Pause", false ));
    gui.add(fitToScreen.set( "Fit to screen", false ));
    gui.add(inverse.set( "Inverse", true ));
    gui.add(mirror.set( "Mirror", 0, 0, 2 ));
    gui.add(kinectOffset1.set( "Kinect offset 1", 0, 0, 100 ));
    gui.add(kinectOffset2.set( "Kinect offset 2", 0, 0, 100 ));
    
    gui.add(hue.set("Hue", 0, 0, 255));
    gui.add(hueRange.set("Hue range", 150, 0, 255));
    gui.add(saturation.set("Saturation", 0, 0, 255));
    gui.add(brightness.set("Brightness", 255, 0, 255));
    gui.add(persistence.set("Persistence", 0.0, 0.0, 1.0));
    
    int oxMin = ofGetScreenWidth()*0.5-640*0.5;
    int oyMin = ofGetScreenHeight()*0.5-480*0.5;
    gui.add(offsetX.set( "Offset X", 0, -oxMin, oxMin )); // offset position of the gray image (silhouette) on the screen
    gui.add(offsetY.set( "Offset Y", 0, -oyMin, oyMin ));
    gui.add(angle.set( "Angle", 10, -30, 30 ));
    gui.add(videoSpeed.set( "Video speed", 1, 0, 2 ));
    gui.add(nearThreshold.set( "Near threshold", 127, 0, 255 ));
    gui.add(farThreshold.set( "Far threshold", 47, 0, 255 ));

    hideGUI = false;
    
    for(uint i=0 ; i<effects.size() ; i++)
    {
        gui.add(effects[i]->parameters);
        effects[i]->hue = &hue;
        effects[i]->hueRange = &hueRange;
        effects[i]->saturation = &saturation;
        effects[i]->brightness = &brightness;
    }
    
    // --- Kinect --- //
    kinect.listDevices();
    
	// enable depth->video image calibration
	kinect.setRegistration(true);
    kinect2.setRegistration(true);

	// kinect.init();
	//kinect.init(true); // shows infrared instead of RGB video image
	kinect.init(); // disable video image (faster fps)
    kinect2.init(); // disable video image (faster fps)

	// kinect.open();		// opens first available kinect
    kinect.open(0);	// open a kinect by id, starting with 0 (sorted by serial # lexicographically))
	kinect2.open(1);	// open a kinect by id, starting with 0 (sorted by serial # lexicographically))
	//kinect.open("A00362A08602047A");	// open a kinect using it's unique serial #
	
    int width = 0;
    int height = 0;
	if(kinect.isConnected() && kinect2.isConnected())
    {
        // print the intrinsic IR sensor values
		ofLogNotice() << "sensor-emitter dist: " << kinect.getSensorEmitterDistance() << "cm";
		ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance() << "cm";
		ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize() << "mm";
		ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance() << "mm";
		kinect.setCameraTiltAngle(angle);
        
		ofLogNotice() << "sensor-emitter dist 2: " << kinect2.getSensorEmitterDistance() << "cm";
		ofLogNotice() << "sensor-camera dist 2:  " << kinect2.getSensorCameraDistance() << "cm";
		ofLogNotice() << "zero plane pixel size 2: " << kinect2.getZeroPlanePixelSize() << "mm";
		ofLogNotice() << "zero plane dist 2: " << kinect2.getZeroPlaneDistance() << "mm";
		kinect2.setCameraTiltAngle(angle);
        
        width = kinect.width * 2;
        height = kinect.height;
	}
	else
	{
		videoPlayer.loadMovie("ShadowDance.mp4");
		videoPlayer.play();
        videoPlayer.update();
        width = videoPlayer.width;
        height = videoPlayer.height;
	}
	
    
    // --- Images --- //
    
	grayImage = cv::Mat(height, width, CV_8U);
	colorImage = cv::Mat(height, width, CV_8UC3);
	grayThreshNear = cv::Mat(height, width, CV_8U);
	grayThreshFar = cv::Mat(height, width, CV_8U);
    
	effectOfImage.allocate(SCREEN_WIDTH, SCREEN_HEIGHT, OF_IMAGE_COLOR);
//	effectOfImage.allocate(width, height, OF_IMAGE_COLOR);
     effectImage = ofxCv::toCv(effectOfImage);
    
    receiver.setup(PORT);
}

void Bodybuilding::readOSC() {
    
	while(receiver.hasWaitingMessages()){
        
		ofxOscMessage m;
		receiver.getNextMessage(&m);
        
		if(m.getAddress() == "/frequencies")
        {
            float valueMax = -1;
            int indexMax = -1;
            map<float, int> freqs;
            int nFreqs = 5;
			for(int i = 0; i < m.getNumArgs(); i++)
            {
                float freqValue = m.getArgAsFloat(0);
                if(freqs.size()<nFreqs)
                    freqs[freqValue] = i;
                else if(freqValue > freqs.rbegin()->first)
                {
                    freqs[freqValue] = i;
                    freqs.erase(freqs.end()--);
                }
            }
            if(effects[effect]->name=="Wave")
            {
                static_cast<Wave *>(effects[effect])->updateWave(freqs);
            }
		}
        else
        {
			// unrecognized message: display on the bottom of the screen
			string msg_string;
			msg_string = m.getAddress();
			msg_string += ": ";
			for(int i = 0; i < m.getNumArgs(); i++){
				// get the argument type
				msg_string += m.getArgTypeName(i);
				msg_string += ":";
				// display the argument - make sure we get the right type
				if(m.getArgType(i) == OFXOSC_TYPE_INT32){
					msg_string += ofToString(m.getArgAsInt32(i));
				}
				else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
					msg_string += ofToString(m.getArgAsFloat(i));
				}
				else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
					msg_string += m.getArgAsString(i);
				}
				else{
					msg_string += "unknown";
				}
			}
            cout << "unknown message" << endl;
            cout << msg_string << endl;
		}
        
	}

}

//--------------------------------------------------------------
void Bodybuilding::update() {
    
	if(persistence>0 && !effects[effect]->drawEffectImage)
    {
        ofSetColor(0,0,0,255*(1.0-persistence));
        ofFill();
        ofRect(0, 0, ofGetScreenWidth(), ofGetScreenHeight());
    }
    else
        ofBackground(0, 0, 0);
	
    bool kinectConnected = kinect.isConnected() && kinect2.isConnected();
    
    int width, height;
    
	if(kinectConnected)
    {
        kinect.update();
        kinect2.update();
        width = kinect.width * 2;
        height = kinect.height;
        
    } else{
        width = videoPlayer.width;
        height = videoPlayer.height;
    }
        
	
	// there is a new frame and we are connected
	if(kinectConnected && kinect.isFrameNew() && kinect2.isFrameNew())
    {
        if(grayImage.cols != width || grayImage.rows != height)
            cv::resize(grayImage, grayImage, cv::Size(width, height));

//      WARNING: DOES NOT WORK SINCE cv::Mat.data returns first pixel of the mat, which is shared by the two submatrices
//      memcpy((void*) grayImage.colRange(0,0).rowRange(0,height).data, (void*) kinect.getDepthPixels(), kinect.height * kinect.width);
//		memcpy((void*) grayImage.colRange(0,width).rowRange(0,height).data, (void*) kinect2.getDepthPixels(), kinect.height * kinect.width);
        
        cv::Mat depth1(kinect.height, kinect.width, CV_8U, kinect.getDepthPixels());
        cv::Mat depth2(kinect.height, kinect.width, CV_8U, kinect2.getDepthPixels());
        
        cv::Mat sub1 = grayImage(cv::Rect(0+kinectOffset1, 0, kinect.width-kinectOffset1, kinect.height));
        cv::Mat sub2 = grayImage(cv::Rect(kinect.width-kinectOffset2, 0, kinect.width-kinectOffset2, kinect.height));
        
        depth1(cv::Rect(0, 0, kinect.width-kinectOffset1, kinect.height)).copyTo(sub1);
        depth2(cv::Rect(kinectOffset2, 0, kinect.width-kinectOffset2, kinect.height)).copyTo(sub2);
               
        //        if(grayImage.cols != kinect.width || grayImage.rows != kinect.height)
        //            cv::resize(grayImage, grayImage, cv::Size(kinect.width , kinect.height));
		// memcpy((void*) grayImage.data, (void*) kinect.getDepthPixels(), kinect.height * kinect.width);
		
        // we do two thresholds - one for the far plane and one for the near plane
		// we then do a cvAnd to get the pixels which are a union of the two thresholds

        grayThreshNear = grayImage.clone();
        grayThreshFar = grayImage.clone();
        
        cv::threshold(grayThreshNear, grayThreshNear, nearThreshold, 255, cv::THRESH_BINARY_INV);
        cv::threshold(grayThreshFar, grayThreshFar, farThreshold, 255, cv::THRESH_BINARY);
        
        grayImage = grayThreshNear & grayThreshFar;
        
        cv::dilate(grayImage, grayImage, cv::Mat(), cv::Point(-1,-1), 6);
        cv::erode(grayImage, grayImage, cv::Mat(), cv::Point(-1,-1), 6);
	}
    else if(!kinectConnected && videoPlayer.width>0 && videoPlayer.height>0)
    {
        if(grayImage.cols != videoPlayer.width || grayImage.rows != videoPlayer.height)
            cv::resize(grayImage, grayImage, cv::Size(videoPlayer.width, videoPlayer.height));

        videoPlayer.setSpeed(videoSpeed);
        videoPlayer.update();
        
		memcpy((void*) colorImage.data, (void*) videoPlayer.getPixels(), videoPlayer.height * videoPlayer.width * 3);
        cv::cvtColor(colorImage,grayImage, CV_BGR2GRAY);
        cv::threshold(grayImage, grayImage, 50, 255, cv::THRESH_BINARY);
    }
    
	if(kinectConnected && (!kinect.isFrameNew() || !kinect.isFrameNew()) )
        return;
    
//    cv::resize(grayImage, grayImageScaled, cv::Size(ofGetScreenWidth(), ofGetScreenHeight()));

    if(effects[effect]->name != "VideoBlend")
    {
        cv::Mat grayImageFull = cv::Mat::zeros(SCREEN_HEIGHT, SCREEN_WIDTH, CV_8U);
        int minX = SCREEN_WIDTH*0.5-width*0.5;
        int minY = SCREEN_HEIGHT*0.5-height*0.5;
        int oxi = offsetX;
        int oyi = offsetY;
        int ox = std::min(minX, std::max(oxi, -minX) );
        int oy = std::min(minY, std::max(oyi, -minY) );
        cv::Mat subGray = grayImageFull(cv::Rect(SCREEN_WIDTH*0.5-width*0.5+ox, SCREEN_HEIGHT*0.5-height*0.5+oy, width, height));
        grayImage.copyTo(subGray);
        
        if(persistence>0.001)
            effectImage = persistence*effectImage;
        else
            effectImage = cv::Mat::zeros(effectImage.size(), effectImage.type());
        
        effects[effect]->update(grayImageFull,effectImage);
        grayImage = grayImageFull.clone();
        
        effectOfImage.update();
    }
    else if(effects[effect]->name == "VideoBlend")
    {
        if(kinectConnected)
            memcpy((void*) colorImage.data, (void*) kinect.getPixels(), kinect.height * kinect.width * 3);
            
        cv::Mat colorImageFull = cv::Mat::zeros(SCREEN_HEIGHT, SCREEN_WIDTH, CV_8UC3);
        int minX = SCREEN_WIDTH*0.5-width*0.5;
        int minY = SCREEN_HEIGHT*0.5-height*0.5;
        int oxi = offsetX;
        int oyi = offsetY;
        int ox = std::min(minX, std::max(oxi, -minX) );
        int oy = std::min(minY, std::max(oyi, -minY) );
        cv::Mat subColor = colorImageFull(cv::Rect(SCREEN_WIDTH*0.5-width*0.5+ox, SCREEN_HEIGHT*0.5-height*0.5+oy, width, height));
        colorImage.copyTo(subColor, grayImage);
        
        effects[effect]->update(colorImageFull,effectImage);

//        grayImage = colorImageFull.clone();
        
        effectOfImage.update();
    }
}

//--------------------------------------------------------------
void Bodybuilding::draw() {
	
	ofSetColor(255, 255, 255);
	
    
    if(mode == 3)           // Light (silhouette) on left side, and effect on right side
    {
        int w = ofGetScreenWidth();
        int h = ofGetScreenHeight();
        ofxCv::drawMat(grayImage, 0, 0, 640, 480);
        effectOfImage.draw(640, 0);
	}
    else if(mode == 2)           // Point cloud
    {
		easyCam.begin();
		drawPointCloud();
		easyCam.end();
	}
    else if (mode == 1)     // Debug
    {
		// draw from the live kinect

        // to be cleaned
        kinect.draw(10, 10, 400, 300);
        kinect2.draw(410, 10, 400, 300);
        
        kinect.drawDepth(10, 310, 400, 300);
        kinect2.drawDepth(410, 310, 400, 300);
        
        ofxCv::drawMat(grayImage, 10, 310*2, 400, 300);
        
        effectOfImage.draw(10, 310*3, 400, 300);
        videoPlayer.draw(10, 310*4, 400, 300);
        
        if(effects[effect]->name == "Particles optical flow")
        {
            ParticlesOptFlow * pEffect = (ParticlesOptFlow *)(effects[effect]);
            cv::Mat flowHSV;
            pEffect->draw_hsv(flowHSV);
            if(flowHSV.cols>0 && flowHSV.rows>0)
                ofxCv::drawMat( flowHSV, 410*2, 10, 400, 300);
            ofxCv::drawMat( pEffect->prevSilhouetteImage, 410*2, 310, 400, 300);
        }
	}
    else if (mode == 0)     // Effects
    {
        int posX = (ofGetWidth()-effectOfImage.width)/2;
        int posY = (ofGetHeight()-effectOfImage.height)/2;
        
        ofPushMatrix();
        ofTranslate(posX, posY, 0);
        effects[effect]->draw();
        ofPopMatrix();
        
        if(effects[effect]->drawEffectImage)
        {
            if(mirror==1)
            {
                cv::Rect r1(0,0,effectImage.cols*0.5,effectImage.rows);
                cv::Rect r2(effectImage.cols*0.5,0,effectImage.cols*0.5,effectImage.rows);
                cv::Mat flipped;
                cv::flip(effectImage(r1), flipped, 1);
                cv::Mat e2 = effectImage(r2);
                flipped.copyTo(e2);
                effectOfImage.update();
            }
            
            if(inverse)
            {
                effectImage = cv::Scalar(255,255,255)-effectImage;
                effectOfImage.update();
            }
            
            if(fitToScreen)
                effectOfImage.draw(0, 0, ofGetWidth(), ofGetHeight());
            else
                effectOfImage.draw(posX, posY);
            
            if(inverse)
                effectImage = cv::Scalar(255,255,255)-effectImage;
        }
    }
	
	// draw instructions
	ofSetColor(255, 255, 255);
	stringstream reportStream;
        
    if(kinect.hasAccelControl()) {
        reportStream << "Accel is: " << ofToString(kinect.getMksAccel().x, 2) << " / "
        << ofToString(kinect.getMksAccel().y, 2) << " / "
        << ofToString(kinect.getMksAccel().z, 2) << endl;
    } else {
        reportStream << "Note: this is a newer Xbox Kinect or Kinect For Windows device," << endl
		<< "motor / led / accel controls are not currently supported" << endl << endl;
    }
    
    fpsLabel = ofToString(ofGetFrameRate());
	reportStream << "press c/p to close/open the connection: " << kinect.isConnected() << endl
	<< "press w to toggle depth near color" << endl
	<< "press h to toggle GUI" << endl;

    if(kinect.hasCamTiltControl()) {
    	reportStream << "press a,z,e,r,t,y to change the led mode" << endl;
    }
    
    if(mode == 1)   // Draw string in debug mode only
        ofDrawBitmapString(reportStream.str(), 20, 652);
    
	if( !hideGUI )
		gui.draw();
    
    mainOutputSyphonServer.publishScreen();
}

void Bodybuilding::drawPointCloud() {
	int w = 640;
	int h = 480;
	ofMesh mesh;
	mesh.setMode(OF_PRIMITIVE_POINTS);
	int step = 2;
	for(int y = 0; y < h; y += step) {
		for(int x = 0; x < w; x += step) {
			if(kinect.getDistanceAt(x, y) > 0) {
				mesh.addColor(kinect.getColorAt(x,y));
				mesh.addVertex(kinect.getWorldCoordinateAt(x, y));
			}
		}
	}
	glPointSize(3);
	ofPushMatrix();
	// the projected points are 'upside down' and 'backwards' 
	ofScale(1, -1, -1);
	ofTranslate(0, 0, -1000); // center the points a bit
	ofEnableDepthTest();
	mesh.drawVertices();
	ofDisableDepthTest();
	ofPopMatrix();
}

//--------------------------------------------------------------
void Bodybuilding::exit() {
	kinect.setCameraTiltAngle(0); // zero the tilt on exit
	kinect.close();
	kinect2.setCameraTiltAngle(0); // zero the tilt on exit
	kinect2.close();
}

//--------------------------------------------------------------
void Bodybuilding::keyPressed (int key) {

    // bool alt = ofGetKeyPressed(OF_KEY_ALT);
    
	switch (key) {
			
		case 'p':
            pause = !pause;
			break;
			
		case OF_KEY_UP:
            if(effect < effect.getMax())
                effect++;
            else
                effect = 0;
			break;
            
		case OF_KEY_DOWN:
            if(effect > 0)
                effect--;
            else
                effect = effect.getMax();
			break;
			
		case 'w':
			kinect.enableDepthNearValueWhite(!kinect.isDepthNearValueWhite());
			kinect2.enableDepthNearValueWhite(!kinect2.isDepthNearValueWhite());
			break;
			
		case 'o':
			kinect.setCameraTiltAngle(angle); // go back to prev tilt
			kinect.open();
			kinect2.setCameraTiltAngle(angle); // go back to prev tilt
			kinect2.open();
			break;
			
		case 'c':
			kinect.setCameraTiltAngle(0); // zero the tilt
			kinect.close();
			kinect2.setCameraTiltAngle(0); // zero the tilt
			kinect2.close();
			break;
			
		case 'a':
			kinect.setLed(ofxKinect::LED_GREEN);
			kinect2.setLed(ofxKinect::LED_GREEN);
			break;
			
		case 'z':
			kinect.setLed(ofxKinect::LED_YELLOW);
			kinect2.setLed(ofxKinect::LED_YELLOW);
			break;
			
		case 'e':
			kinect.setLed(ofxKinect::LED_RED);
			kinect2.setLed(ofxKinect::LED_RED);
			break;
			
		case 'r':
			kinect.setLed(ofxKinect::LED_BLINK_GREEN);
			kinect2.setLed(ofxKinect::LED_BLINK_GREEN);
			break;
			
		case 't':
			kinect.setLed(ofxKinect::LED_BLINK_YELLOW_RED);
			kinect2.setLed(ofxKinect::LED_BLINK_YELLOW_RED);
			break;
			
		case 'y':
			kinect.setLed(ofxKinect::LED_OFF);
			kinect2.setLed(ofxKinect::LED_OFF);
			break;
            
        case 'h':
            hideGUI = !hideGUI;
            break;
        case 'f':
            ofToggleFullscreen();
            break;
	}
}

void Bodybuilding::angleChanged(int &angle)
{
    kinect.setCameraTiltAngle(angle);
    kinect2.setCameraTiltAngle(angle);
}

void Bodybuilding::effectChanged(int & effect)
{
    effectName = effects[effect]->name;
}

void Bodybuilding::pauseToggled(bool &pause)
{
    videoPlayer.setPaused(pause);
}

void Bodybuilding::mouseDragged(int x, int y, int button)
{}

//--------------------------------------------------------------
void Bodybuilding::mousePressed(int x, int y, int button)
{}

//--------------------------------------------------------------
void Bodybuilding::mouseReleased(int x, int y, int button)
{}

//--------------------------------------------------------------
void Bodybuilding::windowResized(int w, int h)
{}
