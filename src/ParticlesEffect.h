//
//  ParticlesEffect.h
//  Bodybuilding
//
//  Created by Arthur Masson on 26/01/2014.
//
//

#ifndef __Bodybuilding__ParticlesEffect__
#define __Bodybuilding__ParticlesEffect__

#include <iostream>
#include "Effect.h"
#include "ofxOsc.h"


class ParticlesEffect : public Effect {
public:
    
    ParticlesEffect(string name):Effect(name)
    {
        particles.resize(5000);
        parameters.add(mask.set("Mask", 0, 0, 4));
        parameters.add(biggestContour.set("Biggest or all contours", false));
        parameters.add(particleSize.set("Particle size", 0, 0, 15));
        parameters.add(nParticles.set("Number of particles", 5000, 100, 15000));
        nParticles.addListener(this, &ParticlesEffect::resizeParticles);
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    void updateContourDirection();
    void resizeParticles(int &nParticles){ particles.resize(nParticles); }
    void drawParticle(int i, ofColor &c, cv::Mat &silhouetteImage, cv::Mat &effectImage, float &prevX, float &prevY, bool drawPrev=true);
    void drawParticle2(cv::Mat &effectImage, cv::Point &prevPoint, cv::Point &point, cv::Scalar &c, bool drawPrev=true);
    
    
    
    ofParameter<bool> biggestContour;
    ofParameter<int> mask;
	ofParameter<int> particleSize;
	ofParameter<int> nParticles;
    vector<cv::Point> contour;
    vector<cv::Point> contourDirections;
    cv::Mat silhouetteContourImage;
    vector<cv::Vec6f> particles;
};

class ParticlesOptFlow : public ParticlesEffect {
public:
    
    
    ofxOscSender sender;
    ofxOscReceiver receiver;
    int noteon_sent;

    
    ParticlesOptFlow(const string name="Particles optical flow"):ParticlesEffect(name)
    {
        c = true;
        
        parameters.add(optFlow.set("Optical flow", true));
        parameters.add(curves.set("Curves", 0.0, 0.0, 0.25));
        parameters.add(speed.set("Speed", 0.3, 0.0, 10.0));
        parameters.add(speedThreshold.set("Speed threshold", 0.0, 0.0, 10.0));
        parameters.add(gravity.set("Gravity", 0.1, -0.5, 0.5));
        parameters.add(timelife.set("Time life", 5.0, 0.0, 100.0));

        curvesShapes.push_back(vector<double>());
        curvesShapes.push_back(vector<double>());
        curvesShapes.push_back(vector<double>());
        curvesShapes.push_back(vector<double>());
        
        nCurveShapes = 4;
        nShapePoints = 6;
        for(int i=0 ; i<nCurveShapes ; i++)
        {
            curvesShapes[i].push_back(0.5);
            for(int j=0 ; j<nShapePoints/2-2 ; j++)
                curvesShapes[i].push_back(ofRandomuf());
            curvesShapes[i].push_back(0.5);
            for(int j=0 ; j<nShapePoints/2 ; j++)
                curvesShapes[i].push_back(1.0-curvesShapes[i][j]);
        }

        for(int i=0 ; i<nParticles.getMax()*2 ; i++)
            points.push_back(ofVec3f(0.0,0.0,0.0));
        
        receiver.setup(4321);
        sender.setup("192.168.2.31", 12345);
        noteon_sent = false;

    }
    void sendFlowToOSC(cv::Mat &effectImage, int widthS, int heightS);

    void computeOpticalFlow(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void updateParticles(int width, int height, double widthS, double heightS);
    void getColorSubpix(const cv::Mat& img, cv::Point2f pt, cv::Vec2f &result);
    void draw_hsv(cv::Mat &flowHSV);
    
    vector <ofVec3f> points;
    cv::Mat prevSilhouetteImage;
    cv::Mat flow;
    cv::Mat previousflow;
    
    int previousSecond;


    
    vector<ofVec3f> prevPoints;
    vector<vector<double> > curvesShapes;
    ofParameter<bool> optFlow;
    ofParameter<float> curves;
	ofParameter<float> speed;
	ofParameter<float> speedThreshold;
	ofParameter<float> gravity;
	ofParameter<float> timelife;
    
    int nCurveShapes;
    int nShapePoints;
    ofShader shader;
    bool c; // bool equals to continuous when object is a ParticlesOptFlowOpenGL, true otherwise
};

class ParticlesOptFlowOpenGL : public ParticlesOptFlow {
public:


    
    ParticlesOptFlowOpenGL(const string name):ParticlesOptFlow(name)
    {
        parameters.add(continuous.set("Continuous", true));
        parameters.add(textureIndex.set("Texture", 0, 0, 1));
        /*
        shader.setGeometryInputType(GL_LINES);
        shader.setGeometryOutputType(GL_TRIANGLE_STRIP);
        shader.setGeometryOutputCount(4);
        shader.load("shaders/vert.glsl", "shaders/frag.glsl", "shaders/geom.glsl");
        */
        // load the texure
        ofDisableArbTex();
        ofLoadImage(texture1, "LensFlare.png");
        ofLoadImage(texture2, "dot.png");
        ofLoadImage(texture3, "line3.png");
        
    }
    
	ofParameter<bool> continuous;
    ofParameter<int> textureIndex;
    
    vector <ofVec3f> sizes;
    ofVbo vbo;
    
    ofTexture texture1;
    ofTexture texture2;
    ofTexture texture3;
};

class StarSphere : public ParticlesOptFlowOpenGL {
public:
    
    StarSphere(const string name="Star sphere"):ParticlesOptFlowOpenGL(name)
    {
        drawEffectImage = false;
        speed.setMax(500.0);
        speed.set(500);
        particleSize.setMax(100.0);
        particleSize.set(6);
        mask.set(2);
        
        int   num = 3000;
        float radius = 300;
        for(int i = 0; i<num; i++ ) {
            
            float theta1 = ofRandom(0, TWO_PI);
            float theta2 = ofRandom(0, TWO_PI);
            
            ofVec3f p;
            p.x = cos( theta1 ) * cos( theta2 );
            p.y = sin( theta1 );
            p.z = cos( theta1 ) * sin( theta2 );
            p *= radius;
            
            points.push_back(p);
            
            // we are passing the size in as a normal x position
            float size = ofRandom(7, 20);
            sizes.push_back(ofVec3f(size));
            
        }
        
        // upload the data to the vbo
        int total = (int)points.size();
        vbo.setVertexData(&points[0], total, GL_STATIC_DRAW);
        vbo.setNormalData(&sizes[0], total, GL_STATIC_DRAW);
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void draw();
    
    ofVec2f rotation;
    ofVec2f rotAcc;
};

class StarFlow : public ParticlesOptFlowOpenGL {
public:
    
    StarFlow(const string name="Star flow"):ParticlesOptFlowOpenGL(name)
    {
        nParticles.setMin(0);
        nParticles.setMax(nParticles);
        
        drawEffectImage = false;
        speed.setMax(0.0);
        speed.setMax(3.0);
        speed.set(0.1);
        gravity.setMin(-0.1);
        gravity.setMax(0.1);
        gravity.set(0.1);
        
        particleSize.setMax(100.0);
        particleSize.set(3);
        
        //        lines.resize(2*nParticles.getMax());
        for(int i = 0; i<nParticles.getMax() ; i++ ) {
            // we are passing the size in as a normal x position
            float size = ofRandom(7, 20);
            sizes.push_back(ofVec3f(size));
        }
        
        // upload the data to the vbo
        int total = (int)points.size();
        vbo.setVertexData(&points[0], total, GL_DYNAMIC_DRAW);
        vbo.setNormalData(&sizes[0], total, GL_DYNAMIC_DRAW);
        
        //        total = (int)lines.size();
        //        linesVbo.setVertexData(&lines[0], total, GL_DYNAMIC_DRAW);
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void drawParticles();
    virtual void draw();
    
    vector <ofVec3f> lines;
    //    ofVbo linesVbo
};


class Wave : public ParticlesOptFlowOpenGL {
public:
    
    Wave(const string name="Wave"):ParticlesOptFlowOpenGL(name)
    {
        lastEmittionTime = 0.0;
        parameters.add(generateFakeFreqs.set("Generate fake frequencies", false));

        nParticles.setMin(0);
        nParticles.setMax(nParticles);
        
        drawEffectImage = false;
        speed.setMax(0.0);
        speed.setMax(3.0);
        speed.set(0.1);
        
        gravity.setName("Wind");
        gravity.setMin(0.0);
        gravity.setMax(1.0);
        gravity.set(0.5);
        
        timelife.setMax(300);
        
        curves.setMax(0.5);
        
        particleSize.setMax(100.0);
        particleSize.set(3);
        
        for(int i = 0; i<nParticles.getMax() ; i++ ) {
            float size = ofRandom(7, 20);
            sizes.push_back(ofVec3f(size));
        }
        
        // upload the data to the vbo
        int total = (int)points.size();
        vbo.setVertexData(&points[0], total, GL_DYNAMIC_DRAW);
        vbo.setNormalData(&sizes[0], total, GL_DYNAMIC_DRAW);
    }
    
    virtual void updateParticles(int width, int height, double widthS, double heightS);
    virtual void updateWave(map<float, int> &freqs);
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    virtual void drawParticles();
    virtual void draw();
    
	ofParameter<bool> generateFakeFreqs;
    
    vector <ofVec3f> lines;
    
    map<float, int> frequencies;
    
    float lastEmittionTime;
    vector<cv::Vec6f*> lastDeadParticles;
};


class ParticlesRain : public ParticlesEffect {
public:
    
    ParticlesRain():ParticlesEffect("Particles rain")
    {
        parameters.add(mode.set("Mode", 0, 0, 2));
        parameters.add(margin.set("Margin", 7, 0, 20));
        
        parameters.add(speed.set("Speed", 0.3, 0.0, 5.0));
        parameters.add(gravity.set("Gravity", 0.1, -1.0, 1.0));
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
    multimap<int,int> sortedContour; // map<point.x, index of point>
    ofParameter<int> mode;
    ofParameter<int> margin;
    
	ofParameter<float> speed;
	ofParameter<float> gravity;
};

class ParticlesGrid : public ParticlesOptFlow {
public:
    
    ParticlesGrid():ParticlesOptFlow("Particles grid")
    {
        parameters.add(strength.set("Strength", 1.5, 0.01, 10.0));
        parameters.add(spring.set("Spring", 0.5, 0.01, 10.0));
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
	ofParameter<float> strength;
	ofParameter<float> spring;
};

class ParticlesCircles : public ParticlesEffect {
public:
    
    ParticlesCircles():ParticlesEffect("Particles circles")
    {
        particles.resize(1000);
        cout << "Building circles..." << endl;
        double startTime = ofGetElapsedTimef();
        
//        descriptorMatcher = cv::DescriptorMatcher::create( "FlannBased" ); // WARNING: APPROXIMATE DO NOT WORK!
        descriptorMatcher = cv::DescriptorMatcher::create( "BruteForce" );

        cv::Mat particlesMat(particles.size(),2, CV_32F);

        randu(particlesMat,0.0,1.0);
        vector<cv::Mat> trainData;
        trainData.push_back(particlesMat);
        descriptorMatcher->add(trainData);
        descriptorMatcher->train();
        
        vector<vector<cv::DMatch> > matches;
        float size = 0.025;
        descriptorMatcher->radiusMatch(particlesMat,matches,size);
        
        for(uint i=0 ; i<particles.size() ; i++)
        {
            float px = particlesMat.at<float>(i,0);
            float py = particlesMat.at<float>(i,1);
            particles[i][0] = px;
            particles[i][1] = py;
            particles[i][2] = 0.0;
        }
        for(uint i=0 ; i<particles.size() ; i++)
        {
            float px = particlesMat.at<float>(i,0);
            float py = particlesMat.at<float>(i,1);
            
            if(used.find(i)!=used.end())
            {
                particles[i][2] = -1.0;
                continue;
            }

            float maxRadius = size;
            for(uint j=0 ; j<matches[i].size() ; j++)
            {
                cv::DMatch &m = matches[i][j];
                int ti = m.trainIdx;
                if(ti==i)
                    continue;
//                double dx = particles[ti][0]-px;
//                double dy = particles[ti][1]-py;
//                double dist = sqrt(dx*dx+dy*dy);
//                if(dist>0.075)
//                    continue;
                map<int,int>::iterator it = used.find(ti);
                if(it!=used.end())
                {
                    int centerParticleIdx = it->second;
                    if(centerParticleIdx==i)
                        continue;
                    double dx = particles[centerParticleIdx][0]-px;
                    double dy = particles[centerParticleIdx][1]-py;
                    double cRadius = particles[centerParticleIdx][2];
                    double dist = sqrt(dx*dx+dy*dy);
                    double cMaxRadius = dist-cRadius;
                    if(cMaxRadius<maxRadius)
                        maxRadius = cMaxRadius;
                }
            }
            for(uint j=0 ; j<matches[i].size() ; j++)
            {
                cv::DMatch &m = matches[i][j];
                int ti = m.trainIdx;
                if(ti==i)
                    continue;
                double dx = particles[ti][0]-px;
                double dy = particles[ti][1]-py;
                double dist = sqrt(dx*dx+dy*dy);
                if(dist<maxRadius)
                    used[ti] = i;
            }
            particles[i][2] = maxRadius;
            used[i] = i;
        }
        
        cout << "Circles built in " << (ofGetElapsedTimef()-startTime) << " seconds." << endl;
    }
    virtual void update(cv::Mat &silhouetteImage, cv::Mat &effectImage);
    
    cv::Ptr<cv::DescriptorMatcher> descriptorMatcher;
    map<int,int> used;
};


#endif /* defined(__Bodybuilding__ParticlesEffect__) */
