#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofxKinect.h"
#include "ofxSyphon.h"
#include "Effect.h"
#include "ParticlesEffect.h"
// #include "OndesPorteesShader.h"
#include "ofxGui.h"
#include "ofxOsc.h"

#define PORT 12345

#define SCREEN_WIDTH 1400
#define SCREEN_HEIGHT 768

class Bodybuilding : public ofBaseApp {
public:
	
	void setup();
	void update();
	void draw();
	void exit();
	
	void drawPointCloud();
	
	void keyPressed(int key);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
    
    // GUI events
    void angleChanged(int & angle);
    void effectChanged(int & effect);
    void pauseToggled(bool &pause);
    
    // OSC
    void readOSC();
	
	ofxKinect kinect;
    ofxKinect kinect2;

	ofVideoPlayer videoPlayer;
	
	cv::Mat effectImage;        // final image
    cv::Mat colorImage;         // color image for video player
    cv::Mat grayImage;          // grayscale depth image
    cv::Mat grayImageScaled;    // grayscale depth image scaled
	cv::Mat grayThreshNear;     // the near thresholded image
	cv::Mat grayThreshFar;      // the far thresholded image
	ofImage effectOfImage;      // effect ofImage (share same data as effectImage, and fast to draw)
    
	ofxPanel gui;
	bool hideGUI;
	ofParameter<bool> pause;
	ofParameter<bool> fitToScreen;
	ofParameter<bool> inverse;
	ofParameter<int> mirror;
	ofParameter<int> kinectOffset1;
	ofParameter<int> kinectOffset2;
    
	ofParameter<int> hueRange;
	ofParameter<int> hue;
	ofParameter<int> saturation;
	ofParameter<int> brightness;
	ofParameter<float> persistence;
    
	ofParameter<int> nearThreshold;
	ofParameter<int> farThreshold;
	ofParameter<int> angle;
	ofParameter<float> videoSpeed;
    
	ofParameter<int> offsetX; // offset position of the gray image (silhouette) on the screen
	ofParameter<int> offsetY;
    
	ofParameter<int> mode;
	ofParameter<int> effect;
	ofParameter<string> effectName;
	ofParameter<string> fpsLabel;
	
	// used for viewing the point cloud
	ofEasyCam easyCam;
    
    vector<Effect*> effects;

    //    ofxPSBlend psBlend;
    
    ofxOscReceiver receiver;
    ofxOscReceiver sender;

	ofxSyphonServer mainOutputSyphonServer;
};
