//
//  Effect.cpp
//  Distorsions
//
//  Created by Arthur Masson on 23/01/2014.
//
//

#include "Effect.h"


void Contours3::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat c1 = silhouetteImage.clone();
    cv::Mat c2 = c1.clone();
    if(inOut)
        cv::erode(c2, c2, cv::Mat(), cv::Point(-1,-1), size);
    else
        cv::dilate(c2, c2, cv::Mat(), cv::Point(-1,-1), size);
    cv::Mat c3 = c2.clone();
    if(inOut)
        cv::erode(c3, c3, cv::Mat(), cv::Point(-1,-1), size);
    else
        cv::dilate(c3, c3, cv::Mat(), cv::Point(-1,-1), size);
    
    vector<cv::Mat> channels;
    cv::Mat rgb(effectImage.rows, effectImage.cols, CV_8UC3);
    cv::split(rgb, channels);
    c1.copyTo(channels[0]);
    c2.copyTo(channels[1]);
    c3.copyTo(channels[2]);
    cv::merge(channels,rgb);
    rgb.copyTo(effectImage,rgb);
}

void ContoursN::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    vector<cv::Mat> cs;
    
    cv::Mat cAll;
    
    if(mode == 2)
        cv::erode(silhouetteImage, silhouetteImage, cv::Mat(), cv::Point(-1,-1), offset);
    
    if(mode == 0 || mode == 2)
        silhouetteImage.convertTo(cAll, CV_32F);
    
    cv::Mat cn = silhouetteImage.clone();
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    cv::Scalar colorCV(color.r, color.g, color.b);
    
    
    for(int i=0; i<n ; i++)
    {
        if(mode == 1)
        {
            cAll = cn.clone();
            vector<vector<cv::Point> > contours;
            cv::findContours(cAll, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
            for(uint j=0 ; j<contours.size() ; j++)
                cv::drawContours( effectImage, contours, j, colorCV, 1, 8);
        }
        if(inOut)
            cv::erode(cn, cn, cv::Mat(), cv::Point(-1,-1), size);
        else
            cv::dilate(cn, cn, cv::Mat(), cv::Point(-1,-1), size);
        if(mode == 0 || mode == 2)
        {
            cv::Mat temp;
            cn.convertTo(temp, CV_32F);
            cAll += temp;
        }
    }
    
    if(mode == 0 || mode == 2)
    {
        double vmin=0;
        double vmax=0;
        minMaxLoc(cAll, &vmin, &vmax);
        
        cAll /= vmax;
        cAll *= 255;
        
        cAll.convertTo(cAll, CV_8U);
        cv::cvtColor(cAll, cAll, CV_GRAY2BGR);
        cAll.copyTo(effectImage,cAll);
//        effectImage = colorCV * effectImage;
    }
}

void ContoursBox::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    // mode: 0:approx || 1:quantized/cubic || 2:hull || 3:bounding box || 4:circle
    
    cv::Mat silhouetteMask;
    if(mask!=0)
        cv::cvtColor(silhouetteImage, silhouetteMask, CV_GRAY2BGR);
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    cv::Scalar colorCV(color.r, color.g, color.b);
    
    float width = silhouetteImage.cols;
    float height = silhouetteImage.rows;
    
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    vector<cv::Point> contourCubic;
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(mode==0 || mode==1)
            cv::approxPolyDP(contours[i], contours[i], precision, true);
        
        if(mode==1 || mode==5)
        {
            for(uint j=0 ; j<contours[i].size() ; j++)
            {
                contours[i][j].x = size*(contours[i][j].x/size);
                contours[i][j].y = size*(contours[i][j].y/size);
            }
        }
        if(mode==2)
        {
            vector<cv::Point> hull;
            cv::convexHull(contours[i], hull);
            for(uint j=1 ; j<hull.size() ; j++)
                cv::line(effectImage, hull[j], hull[j-1], colorCV, std::max(0,(int)thickness));
        }
        if(mode==5)
        {
            int prevX = -1;
            int prevY = -1;
            
            for(uint j=0 ; j<contours[i].size() ; j++)
            {
                int x = size*(contours[i][j].x/size);
                int y = size*(contours[i][j].y/size);
                
                contourCubic.push_back(cv::Point(x,y));
                
                int distX = abs(prevX-x);
                int distY = abs(prevY-y);
                if(prevX>=0 && distX>0 && distY>0)
                    contourCubic.push_back(cv::Point(x,prevY));
                prevX = x;
                prevY = y;
            }
        }
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = contours[i].size();
        }
    }
    if(mode==5)
    {
        vector<vector<cv::Point> > contour;
        contour.push_back(contourCubic);
        cv::drawContours(effectImage, contours, -1, colorCV, thickness);
    }
    if(mode==0 || mode==1)
        cv::drawContours(effectImage, contours, -1, colorCV, thickness);
    
    if(mode==3 && biggestContourIdx>-1 && contours[biggestContourIdx].size()>10)
        cv::rectangle(effectImage, boundingRect(contours[biggestContourIdx]), colorCV, thickness);
    
    if(mode==4 && biggestContourIdx>-1 && contours[biggestContourIdx].size()>10)
    {
        cv::Point2f center;
        float radius;
        cv::minEnclosingCircle(contours[biggestContourIdx], center, radius);
        cv::circle(effectImage, center, radius, colorCV, thickness);
    }
    
    if(mode==6 && biggestContourIdx>-1 && contours[biggestContourIdx].size()>10)
    {
        cv::Rect rect = boundingRect(contours[biggestContourIdx]);
        cv::Point center(rect.x+rect.width*0.5, rect.y+rect.height*0.5);
        cv::circle(effectImage, center, size, colorCV, -1, CV_AA);
    }
    
    if(mask==1)
        silhouetteMask.copyTo(effectImage, silhouetteMask);
    else if(mask==2)
    {
        cv::Mat silhouetteMaskInv = cv::Scalar(255,255,255) - silhouetteMask;
        silhouetteMaskInv.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==3)
    {
        cv::Mat silhouetteMaskDim = 0.5*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==4)
    {
        cv::Mat silhouetteMaskDim = 0.25*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
}

void ContoursSpikes::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{  
    cv::Mat silhouetteCopy = silhouetteImage.clone();
    cv::findContours(silhouetteCopy, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE); // CV_CHAIN_APPROX_TC89_L1);
    
    const int n=5;
//    int nColor = 400;
    for(uint i=0 ; i<contours.size() ; ++i)
    {
        //        drawContours( contourDraw, contours, i, Scalar(0,255,0), 1, 8);
        cv::Point prevPos;
        for(uint j=n ; j<contours[i].size() ; ++j)
        {
            cv::Point avgDir;
            for(int k=0 ; k<n ; k++)
                avgDir += contours[i][j-k-1]-contours[i][j-k];
            
            float s = j/float(contours[i].size());
            if(shape==0)
                s = ofRandomuf();
            else if(shape==1)
                s = 0.5+0.5*sin(s*shapeFrequency);
            else if(shape==2)
                s = 0.5+0.5*ofSign(sin(s*shapeFrequency));
            else if(shape==3)
                s = ((int)(s*shapeFrequency*10)%10)/10.0;
            else if(shape==4)
                s = (abs((int(s*shapeFrequency*10) % 10 - 5)))/5.0;
            else if(shape==5)
                s = abs(sin(s*shapeFrequency));
            
            double size = sizeMin+(sizeMax-sizeMin)*s; // qrand()/double(RAND_MAX);
            double ok = ofRandomuf(); // qrand()/double(RAND_MAX);
            // different ways of choosing the color
            //            double hue = qrand()/double(RAND_MAX);
            //            double hue = 60*(j%nColor)/nColor;
            //            double hue = 128*(1.0+(atan2(avgDir.y, avgDir.x)/3.1416));
            int localHue = abs(int(*hue + *hueRange*(atan2(avgDir.y, avgDir.x)/PI))%255);
            
            color = ofColor::fromHsb(localHue, *saturation, *brightness);
            
            if(ok>amount) // do not display all vectors
                continue;
            
            //            circle(contourDraw, contours[i][j], 2, Scalar(0,0,255));
            cv::Point vhIn = cv::Point(-avgDir.y, avgDir.x);
            cv::Point vhOut = cv::Point(avgDir.y, -avgDir.x);;
            
            double scale = 1.0;
            cv::Point pos = contours[i][j]+size*scale*(inOut?vhIn:1.5*vhOut);
            if(mode==0 || mode==2)
                cv::line(effectImage, contours[i][j], pos, cv::Scalar(color.b, color.g, color.r), 1, CV_AA);
            if((mode==1 || mode==2) && j>1 && prevPos.x>0.1 && prevPos.y>0.1)
                cv::line(effectImage, pos, prevPos, cv::Scalar(color.b, color.g, color.r), 1, CV_AA);
            prevPos = pos;
        }
    }
    if(!inOut)
    {
        cv::Mat ones(silhouetteImage.rows, silhouetteImage.cols, CV_8UC3);
        ones = cv::Scalar(255,255,255);
        ones.copyTo(effectImage, silhouetteImage);
    }
}


void ContoursSmooth::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat silhouetteMask;
    
    
    float width = silhouetteImage.cols;
    float height = silhouetteImage.rows;
    
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = contours[i].size();
        }
    }
    if(contours.size()<=0)
        return;
    
    contour = contours[biggestContourIdx];
    
    cv::approxPolyDP(contour, contour, precision, true);
}

void ContoursSmooth::draw()
{
    if(contour.size()<=0)
        return;
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    ofColor(color);
    
	ofBeginShape();
    ofCurveVertex(contour.back().x, contour.back().y);
    for(uint i=0 ; i<contour.size() ; i++)
    {
        ofCurveVertex(contour[i].x, contour[i].y);
    }
    if(contour.size()<=0)
        return;
    ofCurveVertex(contour[0].x, contour[0].y);
    
	ofEndShape(false);
}

void ContoursLiner::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat silhouetteMask;
    
    float width = silhouetteImage.cols;
    float height = silhouetteImage.rows;
    
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = contours[i].size();
        }
    }
    if(contours.size()<=0)
        return;
    
    contour = contours[biggestContourIdx];
    
    cv::Rect bb = cv::boundingRect(contour);
    cv::Point2i center(bb.x+bb.width*0.5, bb.y+bb.height*0.5);
    cv::approxPolyDP(contour, contour, precision, true);
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    cv::Scalar colorCV(color.r, color.g, color.b);
    
    for(uint i=0 ; i<contour.size() ; i++)
    {
        int x = contour[i].x;
        int y = contour[i].y;
        cv::Point p;
        if(x<center.x)
        {
            if(y<center.y)
            {
                p.x = 0;
                p.y = 0;
            }
            else
            {
                p.x = 0;
                p.y = 768;
            }
        }
        else
        {
            if(y<center.y)
            {
                p.x = 1024;
                p.y = 0;
            }
            else
            {
                p.x = 1024;
                p.y = 768;
            }
        }
        cv::line(effectImage, p, contour[i], colorCV, 1, CV_AA);
        cv::circle(effectImage, contour[i], circleSize, colorCV, -1, CV_AA);
    }
}

void VideoBlend::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    
    cv::Mat silhouetteColor;
    
    if(silhouetteImage.channels()<3)
        cv::cvtColor(silhouetteImage,silhouetteColor,CV_GRAY2BGR);
    else
        silhouetteColor = silhouetteImage;
    silhouetteColor = silhouetteColor*intensity;
    if(prevSilhouetteImage.cols == 0 || prevSilhouetteImage.rows == 0)
    {
        prevSilhouetteImage = silhouetteColor*persistence;
        nUpdate++;
        return;
    }
    
    prevSilhouetteImage.copyTo(effectImage);
    silhouetteColor.copyTo(effectImage, silhouetteColor);

    if(nUpdate>speed)
    {
        nUpdate = 0;
        prevSilhouetteImage = effectImage*persistence;
    }
    nUpdate++;
    /*
    if(prevSilhouetteImage.cols == 0 || prevSilhouetteImage.rows == 0)
    {
        prevSilhouetteImage = silhouetteColor.clone();
        return;
    }
    float alpha = persistence;
    for (int i = 0; i < silhouetteColor.rows; ++i) {
        const uchar* silhouette_i = silhouetteColor.ptr<uchar>(i);
        const uchar* prevSilhouette_i = prevSilhouetteImage.ptr<uchar>(i);
        uchar* effect_i = effectImage.ptr<uchar>(i);
        for (int j = 0; j < silhouetteColor.cols; ++j) {
            effect_i[3*j+0] = uchar(alpha*prevSilhouette_i[3*j+0]+(1.0-alpha)*silhouette_i[3*j+0]);
            effect_i[3*j+1] = uchar(alpha*prevSilhouette_i[3*j+1]+(1.0-alpha)*silhouette_i[3*j+1]);
            effect_i[3*j+2] = uchar(alpha*prevSilhouette_i[3*j+2]+(1.0-alpha)*silhouette_i[3*j+2]);
        }
    }
    
    prevSilhouetteImage = silhouetteColor.clone();
     */
}






