//
//  ParticlesEffect.cpp
//  Bodybuilding
//
//  Created by Arthur Masson on 26/01/2014.
//
//

#include "ParticlesEffect.h"

void ParticlesEffect::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::resize(silhouetteImage, silhouetteImage, cv::Size(0,0),0.2,0.2);
    
    vector<vector<cv::Point> > contours;
    silhouetteContourImage = silhouetteImage.clone();
    cv::findContours(silhouetteContourImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE); // CV_CHAIN_APPROX_TC89_L1);
    
    if(biggestContour)
    {
        int biggestContourIdx = -1;
        int biggestContour = -1;
        
        for(uint i=0 ; i<contours.size() ; i++)
        {
            if(int(contours[i].size())>biggestContour)
            {
                biggestContourIdx = i;
                biggestContour = contours[i].size();
            }
        }
        if(contours.size()<=0)
            return;
        contour.clear();
        contour = contours[biggestContourIdx];
    }
    else
    {
        contour.clear();
        for(uint i=0 ; i<contours.size() ; i++)
        {
            // drawContours( silhouetteImage, contours, i, cv::Scalar(0,255,0), 1, 8);
            contour.insert( contour.end(), contours[i].begin(), contours[i].end() );
        }
    }
}

void ParticlesEffect::updateContourDirection()
{
    contourDirections.clear();
    for(uint i=0 ; i<contour.size() ; i++)
    {
        cv::Point avgDir;
        for(uint k=0 ; k<5 && k+i+1<contour.size() ; k++)
            avgDir += contour[i+k]-contour[i+k+1];
        contourDirections.push_back(cv::Point(-avgDir.y,-avgDir.x));
    }
}

void ParticlesEffect::drawParticle(int i, ofColor &c, cv::Mat &silhouetteImage, cv::Mat &effectImage, float &prevX, float &prevY, bool drawPrev)
{
    int width = effectImage.cols;
    int height = effectImage.rows;
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    cv::Point newPos(width*particles[i][0]/widthS, height*particles[i][1]/heightS);
    
    if(drawPrev)
    {
        cv::Point prevPoint(width*prevX/widthS, height*prevY/heightS);
        cv::line(effectImage, prevPoint, newPos, cv::Scalar(c.r, c.g, c.b), 1, CV_AA);
    }
    cv::circle(effectImage, newPos, particleSize, cv::Scalar(c.r, c.g, c.b), -1, CV_AA);
}

void ParticlesOptFlow::draw_hsv(cv::Mat &flowHSV)
{
    if(flow.cols==0 && flow.rows==0)
        return;
    vector<cv::Mat> channels;
    split( flow, channels );
    cv::Mat fx = channels[0];
    cv::Mat fy = channels[1];
    flowHSV = cv::Mat(flow.rows, flow.cols, CV_8UC3);
    
    for (int i = 0; i < fx.rows; ++i) {
        const float* fx_i = fx.ptr<float>(i);
        const float* fy_i = fy.ptr<float>(i);
        uchar* hsv_i = flowHSV.ptr<uchar>(i);
        for (int j = 0; j < fx.cols; ++j) {
            
            double angle = std::atan2(fy_i[j], fx_i[j]) + 3.1416;
            double strength = 10.0*sqrt(fx_i[j]*fx_i[j]+fy_i[j]*fy_i[j]);
            
            ofColor colorHSL = ofColor::fromHsb(255*angle/(2.0*3.1416), 255, std::min(int(strength), 255));
            
            hsv_i[3*j+0] = colorHSL.r;
            hsv_i[3*j+1] = colorHSL.g;
            hsv_i[3*j+2] = colorHSL.b;
        }
    }
    
    // cvtColor(hsv, flowHSV, COLOR_HSV2BGR);
}

void ParticlesOptFlow::computeOpticalFlow(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    if(prevSilhouetteImage.cols == 0 && prevSilhouetteImage.rows == 0)
        cv::cvtColor(silhouetteImage,effectImage,CV_GRAY2BGR);
    
    ParticlesEffect::update(silhouetteImage, effectImage);
    
    if(prevSilhouetteImage.cols == 0 && prevSilhouetteImage.rows == 0)
    {
        prevSilhouetteImage = silhouetteImage.clone();
        return;
    }
    
    if(contour.size()==0)
        return;
    
    if(optFlow)
        cv::calcOpticalFlowFarneback(prevSilhouetteImage, silhouetteImage, flow, 0.5, 1, 3, 2, 5, 1.2, 0);
    prevSilhouetteImage = silhouetteImage.clone();
}

void ParticlesOptFlow::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat silhouetteMask;
    if(mask!=0)
        cv::cvtColor(silhouetteImage, silhouetteMask, CV_GRAY2BGR);
    
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    computeOpticalFlow(silhouetteImage, effectImage);
    
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    if(!optFlow)
        updateContourDirection();
    
    if(optFlow && flow.cols == 0 && flow.rows == 0)
        return;
    
    if(width == 0 && height == 0)
        return;
    
    if(contour.size()==0)
        return;
    
    for(uint i=0 ; i<particles.size() ; ++i)
    {
        float prevX = particles[i][0];
        float prevY = particles[i][1];
        
        int xi = 0;
        int yi = 0;
        int sizeX = 0;
        
        float dx = particles[i][2];
        float dy = particles[i][3];
        float angle = particles[i][4];
        
        double r1 = ofRandomuf();
        
        // initialize position and speed
        if(particles[i][0] <= 0.0 || particles[i][0] >= widthS || particles[i][1] <= 0.0 || particles[i][1] >= heightS || particles[i][5]<0.0) // if p.y<=0 initialize position
        {
            double r2 = ofRandomuf();
            
            double rx = 0.5*(r1-0.5);
            double ry = 0.5*(r2-0.5);
            
            double cid = double(contour.size())*ofRandomuf();
            int ci = int(cid);
            double cif = cid-ci;
            
            if(ci==int(contour.size())-1)
                ci -= 1;
            
            double x = contour[ci].x*(1.0-cif)+contour[ci+1].x*cif;
            double y = contour[ci].y*(1.0-cif)+contour[ci+1].y*cif;
            particles[i][0] = x+rx;
            particles[i][1] = y+ry;
            particles[i][4] = curves*PI*(ofRandomuf()-0.5);
            
            if(optFlow)
            {
                cv::Vec2f v;
                getColorSubpix(flow, cv::Point2f(x,y), v);
                
                if(sqrt(v[0]*v[0]+v[1]*v[1])<speedThreshold)
                {
                    particles[i][0] = -1.0;
                    particles[i][1] = -1.0;
                }
                
                particles[i][2] = speed*v[0];
                particles[i][3] = speed*v[1];
            }
            else
            {
                cv::Point avgDir = contourDirections[ci];
                particles[i][2] = -speed*avgDir.x;
                particles[i][3] = speed*avgDir.y;
            }
            
            particles[i][5] = 1.0;
        }
        else
        {
            particles[i][0] += 0.5*dx;
            particles[i][1] += 0.5*dy;
            if(curves>0.0)
            {
                particles[i][2] = dx*cos(angle)-dy*sin(angle);
                particles[i][3] = dx*sin(angle)+dy*cos(angle);
            }
            particles[i][3] += gravity;
            particles[i][5] -= r1/timelife;
        }
        
        color = ofColor::fromHsb(*hue,*saturation,*brightness);
        
        drawParticle(i, color, silhouetteImage, effectImage, prevX, prevY, particles[i][5]<0.99);
    }
    
    if(mask==1)
        silhouetteMask.copyTo(effectImage, silhouetteMask);
    else if(mask==2)
    {
        cv::Mat silhouetteMaskInv = cv::Scalar(255,255,255) - silhouetteMask;
        silhouetteMaskInv.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==3)
    {
        cv::Mat silhouetteMaskDim = 0.5*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==4)
    {
        cv::Mat silhouetteMaskDim = 0.25*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
}

void ParticlesOptFlow::getColorSubpix(const cv::Mat& img, cv::Point2f pt, cv::Vec2f &result)
{
    assert(!img.empty());
    assert(img.channels() == 2);
    
    int x = (int)pt.x;
    int y = (int)pt.y;
    
    int x0 = cv::borderInterpolate(x,   img.cols, cv::BORDER_REFLECT_101);
    int x1 = cv::borderInterpolate(x+1, img.cols, cv::BORDER_REFLECT_101);
    int y0 = cv::borderInterpolate(y,   img.rows, cv::BORDER_REFLECT_101);
    int y1 = cv::borderInterpolate(y+1, img.rows, cv::BORDER_REFLECT_101);
    
    float xf = pt.x - (float)x;
    float yf = pt.y - (float)y;
    
    float rx = (img.at<cv::Vec2f>(y0, x0)[0] * (1.f - xf) + img.at<cv::Vec2f>(y0, x1)[0] * xf) * (1.f - yf) + (img.at<cv::Vec2f>(y1, x0)[0] * (1.f - xf) + img.at<cv::Vec2f>(y1, x1)[0] * xf) * yf;
    float ry = (img.at<cv::Vec2f>(y0, x0)[1] * (1.f - xf) + img.at<cv::Vec2f>(y0, x1)[1] * xf) * (1.f - yf) + (img.at<cv::Vec2f>(y1, x0)[1] * (1.f - xf) + img.at<cv::Vec2f>(y1, x1)[1] * xf) * yf;
    
    result = cv::Vec2f(rx,ry);
}

void StarSphere::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = contours[i].size();
        }
    }
    if(contours.size()<=0)
        return;
    
    vector<cv::Point> contourGenuin = contours[biggestContourIdx];
    
    cv::Mat silhouetteMask;
    if(mask!=0)
        cv::cvtColor(silhouetteImage, silhouetteMask, CV_GRAY2BGR);
    
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    computeOpticalFlow(silhouetteImage, effectImage);
    
    contour = contourGenuin;
    
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    if(optFlow && flow.cols == 0 && flow.rows == 0)
        return;
    
    if(width == 0 && height == 0)
        return;
    
    if(contour.size()==0)
        return;
    
    cv::Scalar m = cv::mean(flow);
    m[0] *= -speed;
    m[1] *= -speed;
    if(abs(m[0])>5.0)
        rotAcc.y -= m[0]*0.01;
    else
        rotAcc.y *= 0.97;
    
    if(abs(m[1])>5.0)
        rotAcc.x += m[1]*0.01;
    else
        rotAcc.x *= 0.97;
    
    rotation += rotAcc;
}

void StarSphere::draw()
{
    if(mask>0)
    {
        glDepthMask(GL_TRUE);
        glClear(GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        
        ofDisableAlphaBlending();
        ofDisableBlendMode();
        
        if(contour.size()<=0)
            return;
        
        if(mask==1)
            color = ofColor(255,255,255);
        else if(mask == 2)
            color = ofColor(0,0,0);
        else
            color = ofColor::fromHsb(*hue,*saturation,*brightness);
        
        glColor3f( color.r/255.0, color.g/255.0, color.b/255.0 );
        ofFill();
        
        cv::approxPolyDP(contour, contour, 1, true);
        
        ofBeginShape();
        ofCurveVertex(contour.back().x, contour.back().y);
        for(uint i=0 ; i<contour.size() ; i++)
        {
            ofCurveVertex(contour[i].x, contour[i].y);
        }
        if(contour.size()<=0)
            return;
        ofCurveVertex(contour[0].x, contour[0].y);
        
        ofEndShape(false);
    }

    
    glDepthMask(GL_FALSE);
    glPointSize(particleSize);
    color = ofColor::fromHsb(*hue,*saturation,*brightness);

    glColor3f( color.r/255.0, color.g/255.0, color.b/255.0 );
    
	ofEnableBlendMode(OF_BLENDMODE_ADD);
    
	ofEnablePointSprites();
    
    ofTexture &texture = textureIndex>0?texture2:texture1;
	texture.bind();
    

    glPushMatrix();
    
    ofVec3f yAxis(0,1,0);
    
    ofTranslate(1024*0.5, 768*0.5);
    
    yAxis.rotate(-rotation.x,ofVec3f(1,0,0));
    ofRotateX(rotation.x);
    ofRotate(rotation.y, yAxis.x, yAxis.y, yAxis.z);


	vbo.draw(GL_POINTS, 0, (int)points.size());
    glPopMatrix();
    
    texture.unbind();
	ofDisablePointSprites();
	
	ofDisableBlendMode();
	ofEnableAlphaBlending();
}

void StarFlow::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = contours[i].size();
        }
    }
    if(contours.size()<=0)
        return;
    
    vector<cv::Point> contourGenuin = contours[biggestContourIdx];
    
    cv::Mat silhouetteMask;
    if(mask!=0)
        cv::cvtColor(silhouetteImage, silhouetteMask, CV_GRAY2BGR);
    
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    computeOpticalFlow(silhouetteImage, effectImage);
    
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    if(!optFlow)
        updateContourDirection();
    
    if(optFlow && flow.cols == 0 && flow.rows == 0)
        return;
    
    if(width == 0 && height == 0)
        return;
    
    if(contour.size()==0)
        return;
    
    for(uint i=0 ; i<particles.size() ; ++i)
    {
        float prevX = particles[i][0];
        float prevY = particles[i][1];
        
        int xi = 0;
        int yi = 0;
        int sizeX = 0;
        
        float dx = particles[i][2];
        float dy = particles[i][3];
        float angle = particles[i][4];
        
        double r1 = ofRandomuf();
        
        // initialize position and speed
        if(particles[i][0] <= 0.0 || particles[i][0] >= widthS || particles[i][1] <= 0.0 || particles[i][1] >= heightS || particles[i][5]<0.0) // if p.y<=0 initialize position
        {
            double r2 = ofRandomuf();
            
            double rx = 0.5*(r1-0.5);
            double ry = 0.5*(r2-0.5);
            
            double cid = double(contour.size())*ofRandomuf();
            int ci = int(cid);
            double cif = cid-ci;
            
            if(ci==int(contour.size())-1)
                ci -= 1;
            
            double x = contour[ci].x*(1.0-cif)+contour[ci+1].x*cif;
            double y = contour[ci].y*(1.0-cif)+contour[ci+1].y*cif;
            particles[i][0] = x+rx;
            particles[i][1] = y+ry;
            particles[i][4] = curves*PI*(ofRandomuf()-0.5);
            
            if(optFlow)
            {
                cv::Vec2f v;
                getColorSubpix(flow, cv::Point2f(x,y), v);

                if(sqrt(v[0]*v[0]+v[1]*v[1])<speedThreshold)
                {
                    particles[i][0] = -1.0;
                    particles[i][1] = -1.0;
                }
                
                particles[i][2] = speed*v[0];
                particles[i][3] = speed*v[1];
            }
            else
            {
                cv::Point avgDir = contourDirections[ci];
                particles[i][2] = -speed*avgDir.x;
                particles[i][3] = speed*avgDir.y;
            }
            particles[i][5] = 1.0;
        }
        else
        {
            particles[i][0] += 0.5*dx;
            particles[i][1] += 0.5*dy;
            if(curves>0.0)
            {
                particles[i][2] = dx*cos(angle)-dy*sin(angle);
                particles[i][3] = dx*sin(angle)+dy*cos(angle);
            }
            particles[i][3] += gravity;
            particles[i][5] -= r1/timelife;
        }
        
        //        drawParticle(i, color, silhouetteImage, effectImage, prevX, prevY, particles[i][5]<0.99);
        points[i].x = width*particles[i][0]/widthS;
        points[i].y = height*particles[i][1]/heightS;
        //        ofVertex(width*particles[i][0]/widthS, height*particles[i][1]/heightS, 0.0);
    }
    for(int i=particles.size() ; i<points.size() ; i++)
    {
        points[i].x = 2000.0;
        points[i].y = 2000.0;
    }
    ofVec3f* pointsArray = &points[0];
    vbo.setVertexData(pointsArray, (int)points.size(), GL_DYNAMIC_DRAW);
    
    contour = contourGenuin;
}

void StarFlow::draw()
{
    glClear(GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
    
    if(mask>0)
    {
        if(contour.size()<=0)
            return;
        
        if(mask==1)
            color = ofColor(255,255,255);
        if(mask==2)
            color = ofColor(0,0,0);
        else
            color = ofColor::fromHsb(*hue,*saturation,*brightness);
        
        glColor3f( color.r/255.0, color.g/255.0, color.b/255.0 );
        ofFill();
        cv::approxPolyDP(contour, contour, 1, true);
        
        ofBeginShape();
        ofCurveVertex(contour.back().x, contour.back().y);
        for(uint i=0 ; i<contour.size() ; i++)
        {
            ofCurveVertex(contour[i].x, contour[i].y);
        }
        if(contour.size()<=0)
            return;
        ofCurveVertex(contour[0].x, contour[0].y);
        
        ofEndShape(false);
    }
    
    glPointSize(particleSize);
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    
    glColor3f( color.r/255.0, color.g/255.0, color.b/255.0 );
    
	ofEnableBlendMode(OF_BLENDMODE_ADD);
    
	ofEnablePointSprites();
    
    
    ofTexture &texture = textureIndex>0?texture2:texture1;
	texture.bind();
    
    glPushMatrix();

	vbo.draw(GL_POINTS, 0, (int)points.size());

    glPopMatrix();
    
    
    texture.unbind();
	
    
    ofDisablePointSprites();
	
    
	ofDisableBlendMode();
	ofEnableAlphaBlending();
}

void ParticlesRain::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    cv::Mat silhouetteMask;
    if(mask!=0)
        cv::cvtColor(silhouetteImage, silhouetteMask, CV_GRAY2BGR);
    
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    ParticlesEffect::update(silhouetteImage, effectImage);
    ParticlesEffect::updateContourDirection();
    
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    if(contour.size()==0)
        return;
    
    int minX = silhouetteImage.cols;
    int maxX = 0;
    sortedContour.clear();
    for(uint i=0 ; i<contour.size() ; i++)
    {
        int cx = contour[i].x;
        sortedContour.insert(pair<int,int>(cx, i));
        if(cx<minX)
            minX = cx;
        if(cx>maxX)
            maxX = cx;
    }
    
    minX = std::max(0,minX-margin);
    maxX = std::min(silhouetteImage.cols,maxX+margin);
    
    
    for(uint i=0 ; i<particles.size() ; ++i)
    {
        float &px = particles[i][0];
        float &py = particles[i][1];
        
        float prevX = px;
        float prevY = py;
        float &dx = particles[i][2];
        float &dy = particles[i][3];
        
        float &life = particles[i][5];
        
        // initialize position and speed
        if(px <= 0.0 || px >= widthS || py <= 0.0 || py >= heightS) // if p.y<=0 initialize position
        {
            double r1 = ofRandomuf();
            double r2 = ofRandomuf();
            px = minX + (maxX-minX)*r1;
            py = 0.01;
            dx = 0;
            dy = r2;
            life = 1.0;   // state of the drop: 1.0: just born, 0.9 joung, 0.8: found first contour, 0.7: found second contour
        }
        else
        {
            if(life>0.99)
                life = 0.9;
            
            int gj = px;
            int gi = py;
            
            if(gj<0 || gj>=silhouetteImage.cols || gi<0 || gi>=silhouetteImage.rows)
                continue;
                
            uchar silhouetteAtP = silhouetteImage.at<uchar>(gi,gj);
            
            if(silhouetteAtP>0 && life>0.89 && life<0.91)    // found first contour
            {
                int ci = sortedContour.find(gj)->second;
                if(ci<0 || ci>contourDirections.size())
                    continue;
                cv::Point avgDir = contourDirections[ci];
                dx = -speed*avgDir.x;
                dy = speed*avgDir.y;
                
                life = 0.8;
            }
            else if(silhouetteAtP==0 && life>0.79 && life<0.81 && mode!=2)    // found second contour
            {
                double r1 = ofRandomuf();
                dx = 0.0;
                dy = r1;
                life = 0.9;
            }
            
            px += 0.5*dx;
            py += 0.5*dy;
            dy += gravity;
            
            
            if(px <= 0.0 || px >= widthS || py <= 0.0 || py >= heightS)
                continue;
        }
        color = ofColor::fromHsb(*hue,*saturation,*brightness);
        bool drawPrev = (life<0.99 && life>0.81) || life <0.7;
        if (mode==1 && life<0.99)
            drawPrev = !drawPrev;
        drawParticle(i, color, silhouetteImage, effectImage, prevX, prevY, drawPrev);
    }
    if(mask==1)
        silhouetteMask.copyTo(effectImage, silhouetteMask);
    else if(mask==2)
    {
        cv::Mat silhouetteMaskInv = cv::Scalar(255,255,255) - silhouetteMask;
        silhouetteMaskInv.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==3)
    {
        cv::Mat silhouetteMaskDim = 0.5*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
    else if(mask==4)
    {
        cv::Mat silhouetteMaskDim = 0.25*silhouetteMask;
        silhouetteMaskDim.copyTo(effectImage, silhouetteMask);
    }
}


void ParticlesGrid::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    int width = effectImage.cols;
    int height = effectImage.rows;
    
    ParticlesOptFlow::computeOpticalFlow(silhouetteImage, effectImage);
    
    double widthS = double(silhouetteImage.cols);
    double heightS = double(silhouetteImage.rows);
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    cv::Scalar colorCV(color.r, color.g, color.b);
    
    if(width == 0 && height == 0)
        return;
    
    if(contour.size()==0)
        return;
    
    for(uint i=0 ; i<particles.size() ; ++i)
    {
        double ratio = widthS/double(heightS);
        int sizeX = sqrt(ratio*particles.size());
        int sizeY = sizeX/ratio;
        int xi = i%sizeX;
        int yi = i/sizeX;
        double x = xi*(widthS/double(sizeX));
        double y = yi*(heightS/double(sizeY));
        double dx = particles[i][0]-x;
        double dy = particles[i][1]-y;
        
        if(particles[i][0] == 0 && particles[i][1] == 0)
        {
            particles[i][0] = x;
            particles[i][1] = y;
        }
        else
        {
            double flowX = x;
            double flowY = y;
            cv::Vec2f v;
            getColorSubpix(flow, cv::Point2f(flowX,flowY), v);
            particles[i][0] += strength*v[0]-spring*dx;
            particles[i][1] += strength*v[1]-spring*dy;
        }
        
        cv::Point newPos(width*particles[i][0]/widthS, height*particles[i][1]/heightS);
        
        
        if(xi>0)
        {
            cv::Point posXm1 = cv::Point(width*particles[i-1][0]/widthS, height*particles[i-1][1]/heightS);
            if(posXm1.x>=0 && posXm1.y>=0 && posXm1.x<effectImage.cols && posXm1.y<effectImage.rows && newPos.x>=0 && newPos.y>=0 && newPos.x<effectImage.cols && newPos.y<effectImage.rows)
                cv::line(effectImage, posXm1, newPos, colorCV, 0, CV_AA);
        }
        if(yi>0)
        {
            cv::Point posYm1 = cv::Point(width*particles[i-sizeX][0]/widthS, height*particles[i-sizeX][1]/heightS);
            if(posYm1.x>=0 && posYm1.y>=0 && posYm1.x<effectImage.cols && posYm1.y<effectImage.rows && newPos.x>=0 && newPos.y>=0 && newPos.x<effectImage.cols && newPos.y<effectImage.rows)
                cv::line(effectImage, posYm1, newPos, colorCV, 0, CV_AA);
        }
    }
}

void ParticlesCircles::update(cv::Mat &silhouetteImage, cv::Mat &effectImage)
{
    int width = silhouetteImage.cols;
    int height = silhouetteImage.rows;
    
    color = ofColor::fromHsb(*hue,*saturation,*brightness);
    cv::Scalar colorCV(color.r, color.g, color.b);
    
    
    cv::Mat c = silhouetteImage.clone();
    vector<vector<cv::Point> > contours;
    cv::findContours(c, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
    int biggestContourIdx = -1;
    int biggestContour = -1;
    
    if(contours.size()<=0)
        return;
    
    for(uint i=0 ; i<contours.size() ; i++)
    {
        if(int(contours[i].size())>biggestContour)
        {
            biggestContourIdx = i;
            biggestContour = int(contours[i].size());
        }
    }

    for(uint i=0 ; i<particles.size() ; i++)
    {
        float px = particles[i][0];
        float py = particles[i][1];
        float radius = particles[i][2];
        
        int pxi = px*width;
        int pyi = py*width;
        
        if(pxi<0 || pxi>width || pyi<0 || pyi>height)
            continue;
        
        
        if(radius<0.0)
            continue;
        
//        cv::circle(effectImage, cv::Point(pxi,pyi), 0, cv::Scalar(255,0,0));
        double dist = cv::pointPolygonTest(contours[biggestContourIdx], cv::Point2f(pxi,pyi), false);
        
//        if(dist<0.0)
//            radius = -dist;
//        else
//            continue;
        
        if(dist>0.0)
            continue;
        
        cv::circle(effectImage, cv::Point(pxi,pyi), radius*width, colorCV, 0, CV_AA);
    }
}